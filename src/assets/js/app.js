const contactBtns = document.querySelectorAll('.contactButton')
const contactCloseBtn = document.querySelector('.contactButtonClose')
const contactOverlay = document.getElementById('overlay')

window.onload = function() {
	contactBtns.forEach(btn => {
		btn.addEventListener('click', () => {
			contactOverlay.style.zIndex = 99
			contactOverlay.style.opacity = 1
		})
	})
	
	contactCloseBtn.addEventListener('click', () => {
		contactOverlay.style.zIndex = -99
		contactOverlay.style.opacity = 0
	})
}